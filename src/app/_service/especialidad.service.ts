import { HOST } from './../_shared/var.constant';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Especialidad } from './../_model/especialidad';
import { Injectable, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EspecialidadService {

  especialidades: Especialidad[] = [];
  url: string = `${HOST}/especialidad`;

  constructor(private http: HttpClient) { 

  }

  getListarEspecialidad(){
    return this.http.get<Especialidad[]>(`${this.url}/listar`);
  }
}
